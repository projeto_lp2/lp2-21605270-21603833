package pt.ulusofona.lp2.jungleGame;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class MochoTest {
    
    @Test
    public void testCalcularDistancia() {
        Mocho m = new Mocho("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("T");
        tipoPista.add("T");
        
        
        m.calcularDistancia(2, true, tipoPista);
        int resultado = 6;
        
        int esperado = 6;
        
        
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testCalcularDistancia2() {
        Mocho m = new Mocho("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("A");
        tipoPista.add("R");
        
        
        m.calcularDistancia(2, true, tipoPista);
        int resultado = 6;
        
        int esperado = 6;
        
        
        
        assertEquals(esperado, resultado);
    }
    
}
