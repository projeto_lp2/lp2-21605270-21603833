package pt.ulusofona.lp2.jungleGame;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class TartarugaTest {
    
    @Test
    public void testCalcularDistancia() {
        Tartaruga t = new Tartaruga("ninja", 2, 7, 5, 0, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("T");
        
        
        t.calcularDistancia(3, true, tipoPista);
        int resultado = 0;
        
        int esperado = 0;
        
        
        
        assertEquals(esperado, resultado);
        
        
    }
    
    @Test
    public void testCalcularDistancia2() {
        Tartaruga t = new Tartaruga("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("T");
        tipoPista.add("T");
        
        
        t.calcularDistancia(3, true, tipoPista);
        int resultado = 0;
        
        int esperado = 0;
        
        
        
        assertEquals(esperado, resultado);
        
        
    }
    
    
    
    @Test
    public void testCalcularDistancia3() {
        Tartaruga t = new Tartaruga("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("A");
        
        
        t.calcularDistancia(3, true, tipoPista);
        int resultado = 0;
        
        int esperado = 5;
        
        
        
        assertEquals(esperado, resultado);
        
        
    }
    
}
