package pt.ulusofona.lp2.jungleGame;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class AnimalGeralTest {
    
    @Test
    public void testCalcularDistancia() {
        AnimalGeral a = new AnimalGeral("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("T");
        tipoPista.add("T");
        
        
        a.calcularDistancia(2, true, tipoPista);
        int resultado = 5;
        
        int esperado = 6;
        
        
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testCalcularDistancia2() {
        AnimalGeral a = new AnimalGeral("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("A");
        tipoPista.add("T");
        
        
        a.calcularDistancia(2, true, tipoPista);
        int resultado = 6;
        
        int esperado = 5;
        
        
        
        assertEquals(esperado, resultado);
    }
    
}
