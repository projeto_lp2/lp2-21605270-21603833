 package pt.ulusofona.lp2.jungleGame;

public class Premio {
    private String nome;
    private int valor;
    private int posicao;
    public boolean acumulavel;
  
    
    Premio(String nomePremio, int valor, int posicao, boolean acumulavel){
        this.nome = nomePremio;
        this.valor = valor;
        this.posicao= posicao;
        this.acumulavel = acumulavel;
    }
    
    

    public static String mudarBoolean(boolean acumulavel) {
    return acumulavel ? "S" : "N";
}
    
    public String getNomePremio(){
        return nome;
    }
    
    public int getValorPremio(){
        return valor;
    }

    
    public int getPosicaoPremio() {
        return posicao;
    }
       
}
