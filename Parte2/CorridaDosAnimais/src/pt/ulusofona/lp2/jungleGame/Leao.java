package pt.ulusofona.lp2.jungleGame;

import static java.lang.Math.floor;
import java.util.List;

public class Leao extends Animal {

    public Leao(String nome, int id, int idEspecie, int velocidade, int energiaInicial, int inteligencia) {
        super(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
        this.idEspecie = 4;

    }

    @Override
    public void calcularDistancia(int comprimentoPista, boolean iniciouCorrida, List<String> TipoPista) {

        if (TipoPista.get(posicao).equals("T")) {
            atrito = 0;
        } else {
            atrito = 1;
        }

        int distancia = (int) (velocidade * energiaAtual * 0.5 + 1 - atrito);

        if (posicao + distancia <= comprimentoPista) {
            posicao += distancia;
            int temp = (int) floor(distancia - inteligencia * 1 / 2);
            energiaAtual -= Math.max(temp, 1);
        } else {
            posicao = comprimentoPista;

        }

    }

}
