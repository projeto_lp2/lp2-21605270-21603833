package pt.ulusofona.lp2.jungleGame;

import java.util.List;

public abstract class  Animal {   //abstract

    protected String nome;
    protected int id;
    protected int idEspecie;
    protected int velocidade;
    protected int energiaInicial;
    protected int energiaAtual;
    protected int posicao;
    private String nomeEspecie;
    protected int tempo;
    protected int classificacao;
    protected final int inteligencia;
    protected int atrito;
    protected boolean voador;
    protected int nrPontos;
    protected int favoritismo;
    public List<String> TipoPista;
    public List<Premio> premios;

    Animal(String nome, int id, int idEspecie, int velocidade, int energiaInicial, int inteligencia) {
        this.nome = nome;
        this.id = id;
        this.idEspecie = idEspecie;
        this.velocidade = velocidade;
        this.energiaInicial = energiaInicial;
        this.posicao = 0;
        this.energiaAtual = energiaInicial;
        this.nomeEspecie = nomeEspecie;
        this.tempo = tempo;
        this.classificacao = classificacao;
        this.premios = premios;
        this.inteligencia = inteligencia;
        this.atrito = atrito;
        this.voador = voador;
        this.favoritismo = favoritismo;
        this.nrPontos = 0;
    }

    public String verificarIdEspecie() {
        switch (idEspecie) {
            case 0:
                nomeEspecie = "Águia";
                break;
            case 1:
                nomeEspecie = "Girafa";
                break;
            case 2:
                nomeEspecie = "Gorila";
                break;
            case 3:
                nomeEspecie = "Humano";
                break;
            case 4:
                nomeEspecie = "Leão";
                break;
            case 5:
                nomeEspecie = "Lebre";
                break;
            case 6:
                nomeEspecie = "Mocho";
                break;
            case 7:
                nomeEspecie = "Tartaruga";
                break;
            case 8:
                nomeEspecie = "Tigre";
                break;
            case 9:
                nomeEspecie = "Zebra";
                break;
        }
        return nomeEspecie;
    }



    public abstract void calcularDistancia(int comprimentoPista, boolean iniciouCorrida, List<String> TipoPista);
    

     
    public String getNomeEspecie(){
        return nomeEspecie;
    }
    
    
    public int getPosicao(){
        return posicao;
    }
  
  /* private int compareToEmpates(Animal animal1, Animal animal2){
       
    //Teste
       
        if(animal1.favoritismo > animal2.favoritismo ){
            return 1;
        }else{
            return 0;
        }
        
    }*/
   
/*private void atribuirPremios(){
     
     if(classificacao == 1){
         premios.get(posicao);
     }
     
    
 }*/
       
   
   
   
   
   
    public String getImagePNG(){
        
      /*  String tarzan = "Tarzan.png";
        String mocho = "Mocho.png";
        String leao = "Leao.png";
        
        if(idEspecie == 4){
            return leao;
        }else if(idEspecie == 6){
            return mocho;
        }else if(idEspecie == 3){
            return tarzan;
        }else{
            return tarzan;
        }
      */
      return null;
        
    }
    
    public String getNomeAnimal(){
        return nome;
    }
    
    
    public int getVelocidade(){
        return velocidade;  
    }
    
    
    public int getPosicaoAnimal(){
        return posicao;
    }
    
    
    public int getEnergiaAtual(){
        return energiaAtual;
    }
    
    
  public int getTempo(){
        return tempo;
    }

}
  
 
