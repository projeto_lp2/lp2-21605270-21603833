package pt.ulusofona.lp2.jungleGame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Simulador {

    List<Animal> animais = new ArrayList<>();
    List<Premio> premios = new ArrayList<>();
    List<Animal> meta = new ArrayList<>();
    List<String> tabelaPremios = new ArrayList<>();
    List<String> classificacaoGeral = new ArrayList<>();
    List<String> classificacaoEquipas = new ArrayList<>();
    List<String> TipoPista = new ArrayList<>();
    boolean iniciouCorrida = false;
    int numAnimais = 0;
    int comprimentoPista = 0;
    int ultimaPosicao = 1;

    public boolean iniciaJogo(File ficheiroInicial, int comprimentoPista) {

        this.comprimentoPista = comprimentoPista;
        return lerFicheiro(ficheiroInicial);

    }

    public boolean lerFicheiro(File ficheiroInicial) {
        String nomeFicheiro = "Input.txt";

        try {
            File ficheiro = new File(nomeFicheiro);
            try (Scanner leitorFicheiro = new Scanner(ficheiro)) {
                int numLinha = 0;
                int numPremios = 0;

                // enquanto o ficheiro tiver linhas não-lidas
                while (leitorFicheiro.hasNextLine()) {
                    String linha = leitorFicheiro.nextLine();

                    String[] dados = linha.split(":");
                    System.out.println(dados.length);
                    if (numLinha == 0) {
                        numAnimais = Integer.parseInt(dados[0]);
                        numPremios = Integer.parseInt(dados[1]);
                        numLinha++;
                    } else if (numLinha <= numAnimais) {
                        String nome = dados[0];
                        int id = Integer.parseInt(dados[1]);
                        int idEspecie = Integer.parseInt(dados[2]);
                        int velocidade = Integer.parseInt(dados[3]);
                        int energiaInicial = Integer.parseInt(dados[4]);
                        int inteligencia = Integer.parseInt(dados[5]);
                        Animal animal;
                        switch (idEspecie) {
                            case 0:
                                animal = new Aguia(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 1:
                                animal = new Girafa(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 2:
                                animal = new Gorila(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 4:
                                animal = new Leao(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 6:
                                animal = new Mocho(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 7:
                                animal = new Tartaruga(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            default:
                                animal = new AnimalGeral(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                        }
                        animais.add(animal); // guarda na lista

                        numLinha++;

                    } else if (numLinha < 9) {
                        String nome = dados[0];
                        int valor = Integer.parseInt(dados[1]);
                        int posicao = Integer.parseInt(dados[2]);
                        boolean acumulavel = Boolean.parseBoolean(dados[3]);
                        Premio premio = new Premio(nome, valor, posicao, acumulavel);
                        premios.add(premio);
                        numLinha++;
                        System.out.println(numLinha);

                    } else if (numLinha == 9) {
                        dados = linha.split("");
                        for (String dado : dados) {
                            TipoPista.add(dado);
                        }
                    }

                }
            }

        } catch (FileNotFoundException exception) {
            String mensagem = "Erro: o ficheiro " + nomeFicheiro + " não foi encontrado.";
            System.out.println(mensagem);
            return false;
        }

        return true;
    }

    public boolean processaTurno() {

        int numJogadores = animais.size();
        int count = 0;

        for (int i = 0; i < numJogadores; i++) {

            if (meta.contains(animais.get(i)) == false) {

                if (animais.get(i).getEnergiaAtual() > 0) {

                    animais.get(i).calcularDistancia(comprimentoPista - 1, iniciouCorrida, TipoPista);
                    animais.get(i).tempo++;

                    if (animais.get(i).getPosicao() == comprimentoPista - 1) {
                        animais.get(i).classificacao += ultimaPosicao;              //ultimaPosicao da pista
                        ultimaPosicao++;
                        meta.add(animais.get(i));
                    }

                } else if (animais.get(i).energiaAtual == 0) {

                    int Temp = Math.min(animais.get(i).inteligencia * 1 / 2, animais.get(i).energiaInicial);

                    animais.get(i).energiaAtual += Math.max(Temp, 1);

                    animais.get(i).tempo++;

                }
            }
        }
        iniciouCorrida = true;

        for (Animal a : animais) {

            if (a.getPosicaoAnimal() >= comprimentoPista - 1) {
                count++;
            }
        }

        return count != numJogadores;
    }
    

    public List<String> getClassificacaoGeral() {

        String mensagem = " ";
        String titulo = " ";

        classificacaoGeral = new ArrayList<>();

        titulo = "CLASSIFICACAO GERAL";

        for (int i = 0; i < animais.size(); i++) {
            mensagem = "#" + animais.get(i).classificacao + " " + animais.get(i).getNomeAnimal() + ", " + animais.get(i).verificarIdEspecie() + ", " + animais.get(i).getTempo();
            classificacaoGeral.add(mensagem);
            Collections.sort(classificacaoGeral);
        }
        classificacaoGeral.add(0, titulo);
        return classificacaoGeral;
    }

    public String concatenarPremios() {

        String linha = "";
        for (Premio premio : premios) {
            linha += " " + premio.getNomePremio() + ", " + premio.getValorPremio() + "\n";
        }
        return linha;
    }

    public List<String> getTabelaPremios() {

        String mensagem = "";
        String nomeAnimal;
        String premiosAnimal;
        
        for(Animal animal : meta){
            nomeAnimal = animal.nome;
            premiosAnimal = concatenarPremios();
            tabelaPremios.add(nomeAnimal + "\n" + premiosAnimal + "\n");
        }
 
        return tabelaPremios;
    }

    public List<String> getClassificacaoEquipas() {

        classificacaoEquipas = new ArrayList<>();
        String titulo = " ";
        String mensagem = " ";

        for (int i = 0; i < meta.size(); i++) {
            animais.get(i).nrPontos = numAnimais - animais.get(i).classificacao + 1;
        }
        titulo = "CLASSIFICACAO POR EQUIPAS";

        for (int i = 0; i < meta.size(); i++) {

            mensagem = "#" + animais.get(i).classificacao + " " + animais.get(i).verificarIdEspecie() + ", " + animais.get(i).nrPontos;
            classificacaoEquipas.add(mensagem);
            Collections.sort(classificacaoEquipas);

        }
        classificacaoEquipas.add(0, titulo);
        System.out.println("" + classificacaoEquipas);
        return classificacaoEquipas;
    }

    public void escreverResultados(String filename) {

        try {
            filename = "Resultados.txt";
            FileWriter fileWriter = new FileWriter(filename);

            for (int i = 0; i < classificacaoGeral.size(); i++) {
                fileWriter.write(classificacaoGeral.get(i));
                fileWriter.flush();
            }

            for (int i = 0; i < classificacaoEquipas.size(); i++) {
                fileWriter.write(classificacaoEquipas.get(i));
                fileWriter.flush();
            }
            fileWriter.close();

        } catch (IOException e) {
            String mensagem = "Erro: o ficheiro " + filename + " não foi encontrado.";
            System.out.println(mensagem);
        }
    }

    public String getBackgroundImagePNG(int idPosicao) {

        String terra = "box-land.png";
        String agua = "box-water.png";
        String lama = "box-mud.png";
        String rochoso = "box-rocks.png";

        if (TipoPista.contains("T")) {
            switch (TipoPista.get(idPosicao)) {
                case "T":
                    return terra;
                case "A":
                    return agua;
                case "L":
                    return lama;
                case "R":
                    return rochoso;
            }
        }
        return terra;
    }

    public List<Animal> getAnimais() {
        return animais;
    }

    public List<Premio> getPremios() {
        return premios;
    }

}
