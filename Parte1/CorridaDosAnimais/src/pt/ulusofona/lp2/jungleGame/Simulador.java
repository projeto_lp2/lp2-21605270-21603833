package pt.ulusofona.lp2.jungleGame;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;



public class Simulador {
    
    List<Animal> animais = new ArrayList<>();
    List<Premio> premios = new ArrayList<>();
    List<Animal> classificacao = new ArrayList<>();
    List<String> classificacaoFinal = new ArrayList<>();
    List<String> classificacaoGeral = new ArrayList<>();
    int comprimentoPista = 0;
    int ultimaPosicao = 1;
    

    public boolean iniciaJogo(File ficheiroInicial, int comprimentoPista){
        
        this.comprimentoPista = comprimentoPista;
        return lerFicheiro(ficheiroInicial);
        
    }
    
    
    public boolean lerFicheiro(File ficheiroInicial){
        String nomeFicheiro = "Input.txt";
        
        try {
            File ficheiro = new File(nomeFicheiro);
            Scanner leitorFicheiro = new Scanner(ficheiro);
            int numLinha = 0;
            int numAnimais = 0;
            int numPremios = 0;
            // enquanto o ficheiro tiver linhas não-lidas
            
            while(leitorFicheiro.hasNextLine()) {
                
                String linha = leitorFicheiro.nextLine();
            
                String dados[] = linha.split(":");
                    if(numLinha == 0){
                        numAnimais = Integer.parseInt(dados[0]);
                        numPremios = Integer.parseInt(dados[1]);
                        numLinha++;
                    }
                    else if(numLinha <= numAnimais){
                        String nome = dados[0];
                        int id = Integer.parseInt(dados[1]);
                        int idEspecie =Integer.parseInt(dados[2]);
                        int velocidade = Integer.parseInt(dados[3]);
                        int energiaInicial = Integer.parseInt(dados[4]);
                        Animal animal = new Animal(nome, id, idEspecie, velocidade, energiaInicial);
                        animais.add(animal); // guarda na lista

                        numLinha++;
                
                    }else if(numLinha <= numPremios){
                        String nome = dados[0];
                        int valor = Integer.parseInt(dados[1]);
                        int posicao = Integer.parseInt(dados[2]);
                        
                        Premio premio = new Premio(nome, valor, posicao);
                        premios.add(premio);
                        numLinha++;
                    }
                
                }
            leitorFicheiro.close();
            
        }
        
        catch(FileNotFoundException exception){
            String mensagem = "Erro: o ficheiro " + nomeFicheiro + " não foi encontrado.";
            System.out.println(mensagem);
            return false;
        }
   
        return true;
        }

    
    public List<Animal> getAnimais(){
        return animais;
    }
    
    
    public List<Premio> getPremios(){
        return premios;
    }
    

    public boolean processaTurno(){
        
        int numJogadores = animais.size();
        int count = 0;
       

        for(int i=0; i < numJogadores; i++){
           
            if(classificacao.contains(animais.get(i))== false){
            
                if(animais.get(i).getEnergiaAtual() > 0 ){
                    animais.get(i).calcularDistancia(comprimentoPista-1);
                    animais.get(i).tempo++;
                
                    if(animais.get(i).getPosicao() == comprimentoPista-1){
                        animais.get(i).classificacao += ultimaPosicao;
                        ultimaPosicao++;
                        classificacao.add(animais.get(i));
                    }

                }
                else if(animais.get(i).energiaAtual == 0){
                    animais.get(i).energiaAtual += 2;
                    animais.get(i).tempo++;
                }
            } 
        }
              
        for(Animal a : animais){
            
            if(a.getPosicaoAnimal() >= comprimentoPista-1){
                count++;
            }
        }
       
        return count != numJogadores;
    }

    
    public List<String> getClassificacaoGeral(){
     
        String mensagem=  null;
        classificacaoGeral = new ArrayList<>();
     
        for(int i = 0 ; i < animais.size() ; i ++){
            mensagem = "#"+ animais.get(i).classificacao + " " + animais.get(i).getNomeAnimal() + ", " + animais.get(i).verificarIdEspecie() + ", " + animais.get(i).getTempo();
            classificacaoGeral.add(mensagem);
            Collections.sort(classificacaoGeral);
            
        }
     
        return classificacaoGeral;
     
    }
 
    
    public List<String> getTabelaPremios(){
        
        String mensagem = null;
        
       /* for(int i = 0; i < classificacao.size(); i ++){
            int count = 0;
            for(int j= 0 ; j < premios.size() ; j++){
                if(premios.get(i).getPosicaoPremio() == classificacao.get(j).getPosicao()){
                    if(count == 0){
                        mensagem = classificacao.get(j).getNomeAnimal() + "\n   "  + premios.get(i).getNomePremio() + "(" + premios.get(i).getPosicaoPremio() + ")" + "\n";
                    }else{
                        mensagem += premios.get(i).getNomePremio() + "(" + premios.get(i).getPosicaoPremio() + ")" + "\n";
                    }
                classificacaoFinal.add(mensagem);
                count++;
                }
            }
        }
       */
        return classificacaoFinal;
    }

    
    public void escreverResultados(String filename){
        
        try{
            filename = "Resultados.txt";
            FileWriter fileWriter = new FileWriter(filename);
            
            for(int i = 0; i < classificacaoFinal.size(); i ++){
                
                fileWriter.write(classificacaoFinal.get(i));
                fileWriter.flush();
                
            }
            fileWriter.close();
            
        } catch(IOException e){
            String mensagem = "Erro: o ficheiro " + filename + " não foi encontrado.";
            System.out.println(mensagem);
        }
    }
}