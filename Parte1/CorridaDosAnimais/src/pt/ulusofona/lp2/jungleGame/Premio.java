package pt.ulusofona.lp2.jungleGame;

public class Premio {
    String nome;
    int valor;
    int posicao;
  
    
    Premio(String nomePremio, int valor, int posicao){
        this.nome = nomePremio;
        this.valor = valor;
        this.posicao= posicao;
    }
    
    
    public String getNomePremio(){
        return nome;
    }
    
    public int getValorPremio(){
        return valor;
    }

    
    public int getPosicaoPremio() {
        return posicao;
    }
       
}
