package pt.ulusofona.lp2.jungleGame;


public class Animal {
    String nome;
    int id;
    int idEspecie;
    int velocidade;
    int energiaInicial;
    int energiaAtual;
    int posicao;
    String nomeEspecie;
    int tempo;
    int classificacao;
    Premio premio;
  
    
    Animal(String nome, int id, int idEspecie,  int velocidade,  int energiaInicial){
        this.nome = nome;
        this.id= id;
        this.idEspecie= idEspecie;
        this.velocidade = velocidade;
        this.energiaInicial = energiaInicial;
        this.posicao = 0;
        this.energiaAtual = energiaInicial;
        this.nomeEspecie = nomeEspecie;
        this.tempo= tempo;
        this.classificacao = classificacao;
        this.premio = premio;
    }
    
    
    public String verificarIdEspecie(){ 
        switch (idEspecie){
            case 0: 
                nomeEspecie = "Águia";
                break;
            case 1: 
                nomeEspecie = "Girafa";
                break;
            case 2: 
                nomeEspecie = "Gorila";
                break;
            case 3: 
                nomeEspecie = "Humano";
                break;
            case 4: 
                nomeEspecie = "Leão";
                break;
            case 5: 
                nomeEspecie = "Lebre";
                break;
            case 6:
                nomeEspecie = "Mocho";
                break;
            case 7: 
                nomeEspecie = "Tartaruga";
                break;
            case 8: 
                nomeEspecie = "Tigre";
                break;
            case 9: 
                nomeEspecie = "Zebra";
                break;
        }
    
        return nomeEspecie;
    }
    
    
    public String getNomeEspecie(){
        return nomeEspecie;
    }
  
    
  
    public void calcularDistancia(int comprimentoPista){
        
        int distancia = (int)(velocidade * energiaAtual * 0.5 + 1);
        
        if(posicao + distancia <= comprimentoPista){
            posicao += distancia;
            energiaAtual --;
            
              
             
        }else{
            posicao = comprimentoPista;
            
        }
    }
  
    
    public int getPosicao(){
        return posicao;
    }
  
    
    public String getImagePNG(){
        return null;
    }
    
    
    public String getNomeAnimal(){
        return nome;
    }
    
    
    public int getVelocidade(){
        return velocidade;  
    }
    
    
    public int getPosicaoAnimal(){
        return posicao;
    }
    
    
    public int getEnergiaAtual(){
        return energiaAtual;
    }
    
    
  public int getTempo(){
        return tempo;
    }
   
}
  
 
