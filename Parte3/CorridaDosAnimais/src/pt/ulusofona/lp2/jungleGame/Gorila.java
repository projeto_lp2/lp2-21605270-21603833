package pt.ulusofona.lp2.jungleGame;

import static java.lang.Math.floor;
import static java.lang.Math.max;
import java.util.List;

public class Gorila extends Animal {

    public Gorila(String nome, int id, int idEspecie, int velocidade, int energiaInicial, int inteligencia) {
        super(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
        this.idEspecie = 2;
    }

    @Override
    public void calcularDistancia(int comprimentoPista, boolean iniciouCorrida, List<String> TipoPista) {
        

        if ("R".equals(TipoPista) || "T".equals(TipoPista)) {
            atrito = 0;
        } else {
            atrito = 1;
        }

        int distancia = (int) (velocidade * energiaAtual * 0.5 + 1 - atrito);

        if (posicao + distancia <= comprimentoPista) {
            posicao += distancia;
            int temp = (int) floor(distancia - inteligencia * 0.5);

            System.out.println("Distancia: " + distancia + "|-|" + "Temp: " + temp);
            energiaAtual -= Math.max(temp, 1);
            System.out.println("" + energiaAtual);

        } else {
            posicao = comprimentoPista;

        }
    }
}
