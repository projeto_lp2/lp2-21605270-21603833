/*package pt.ulusofona.lp2.jungleGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Equipa implements Comparable<Equipa> {
    int idEspecie;
    int pontos;
    int posicao;
    int favoritismo;
    String nomeEspecie;
    List<Animal> animais;
    
    Equipa(){        
        animais = new ArrayList<>();
    }
    
    public void adicionarPontos(int valor){
        pontos += valor;
    }
    
    public void adicionarPosicao(int valor){
        posicao += valor;
    }
    
    public void adicionarAnimal(Animal animal){
        animais.add(animal);
        idEspecie = animal.getIdEspecie();
        nomeEspecie = animal.getNomeEspecie();
    }
    
    public void calcularfavoritismo(){
        
        for(int i = 0; i<2 && i < animais.size(); i++){
                if(animais.get(i) != null){
                    this.favoritismo += animais.get(i).getFavoritismo();
                }
        }
    }
    
    public void calcularPontos(int numAnimais){
        for(int i = 0; i<2 && i < animais.size(); i++){
            this.pontos += numAnimais - animais.get(i).getPosicaoDeChegada() + 1;
        }
    }
    
    public void ordenar(){
        Animal.selecionarOrdenacao(-1);
        Collections.sort(animais);
    }
    
    @Override
    public int compareTo(Equipa outro){
       
        if (this.pontos == outro.pontos) {
            if(this.favoritismo == outro.favoritismo){
                    if(this.nomeEspecie.compareTo(outro.nomeEspecie) == 0){
                        return 0;
                    }else if(this.nomeEspecie.compareTo(outro.nomeEspecie) < 0){
                        return -1;
                    } else {
                        return 1;
                    }
                }else if(this.favoritismo < outro.favoritismo){ 
                    return -1;
                } else {
                    return 1;
                }
        }else if (this.pontos > outro.pontos) {
            return -1;
        } else {
            return 1;
        }
    }
    
    public String imprimirEquipa(){
        
        return "#" + posicao +" "+ nomeEspecie + ", " + pontos + "\n";
    }
    
    
    
}
*/