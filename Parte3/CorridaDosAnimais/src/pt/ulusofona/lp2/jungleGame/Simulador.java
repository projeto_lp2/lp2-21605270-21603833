package pt.ulusofona.lp2.jungleGame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Simulador {

    private final List<Animal> animais = new ArrayList<>();
    private final List<Premio> premios = new ArrayList<>();
    private final List<Animal> meta = new ArrayList<>();
    private final List<String> tabelaPremios = new ArrayList<>();
    private List<String> classificacaoGeral = new ArrayList<>();
    private List<String> classificacaoEquipas = new ArrayList<>();
    private final List<String> TipoPista = new ArrayList<>();
    private final List<Fenomeno> fenomenos = new ArrayList<>();
    private boolean iniciouCorrida = false;
    private int idFenomenos;
    private int numAnimais = 0;
    private int comprimentoPista = 0;
    private int ultimaPosicao = 1;
    private int countTurno= 0;

   public void iniciaJogo(File ficheiroInicial, int comprimentoPista) throws InvalidSimulatorInputException {

        this.comprimentoPista = comprimentoPista;
        lerFicheiro(ficheiroInicial);

    }

   public void lerFicheiro(File ficheiroInicial) throws InvalidSimulatorInputException {
        String nomeFicheiro = "Input.txt";

        try {
            File ficheiro = new File(nomeFicheiro);
            try (Scanner leitorFicheiro = new Scanner(ficheiro)) {
                int numLinha = 0;
                int numPremios = 0;

                // enquanto o ficheiro tiver linhas não-lidas
                while (leitorFicheiro.hasNextLine()) {
                    String linha = leitorFicheiro.nextLine();
                    String[] dados = linha.split(":");
					
                    if (numLinha == 0) {
                        if (dados.length != 2) {
                            InvalidSimulatorInputException e = new InvalidSimulatorInputException();
                            e.getInputLine(numLinha);
                        }
                        numAnimais = Integer.parseInt(dados[0]);
                        numPremios = Integer.parseInt(dados[1]);
                        numLinha++;
                    } else if (numLinha <= numAnimais) {
                        if (dados.length != 6) {
                            InvalidSimulatorInputException e = new InvalidSimulatorInputException();
                            e.getInputLine(numLinha);
                        }
                        String nome = dados[0];
                        int id = Integer.parseInt(dados[1]);
                        int idEspecie = Integer.parseInt(dados[2]);
                        int velocidade = Integer.parseInt(dados[3]);
                        int energiaInicial = Integer.parseInt(dados[4]);
                        int inteligencia = Integer.parseInt(dados[5]);
                        Animal animal;
                        switch (idEspecie) {
                            case 0:
                                animal = new Aguia(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 1:
                                animal = new Girafa(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 2:
                                animal = new Gorila(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 4:
                                animal = new Leao(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 6:
                                animal = new Mocho(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            case 7:
                                animal = new Tartaruga(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                            default:
                                animal = new AnimalGeral(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
                                break;
                        }
                        animais.add(animal);

                        numLinha++;

                    } else if (numLinha < 9) {
                        if (dados.length != 4) {
                            InvalidSimulatorInputException e = new InvalidSimulatorInputException();
                            e.getInputLine(numLinha);
                        }
                        String nome = dados[0];
                        int valor = Integer.parseInt(dados[1]);
                        int posicao = Integer.parseInt(dados[2]);
                        String acumulavel = (dados[3]);
                        Premio premio = new Premio(nome, valor, posicao, acumulavel);
                        premios.add(premio);
                        numLinha++;
                        System.out.println(numLinha);

                    } else if (numLinha < 13) {
                        if (dados.length != 2) {
                            InvalidSimulatorInputException e = new InvalidSimulatorInputException();
                            e.getInputLine(numLinha);
                        }
                        int idFenomenos = Integer.parseInt(dados[0]);
                        int turno = Integer.parseInt(dados[1]);
                        Fenomeno fenomeno = new Fenomeno(idFenomenos, turno);
                        fenomenos.add(fenomeno);
                        numLinha++;
                        
                    } else if (numLinha == 13) {
                        dados = linha.split("");
                        for (String dado : dados) {
                            TipoPista.add(dado);
                        }
                    }

                }
            }
        } catch (FileNotFoundException exception) {
            String mensagem = "Erro: o ficheiro " + nomeFicheiro + " não foi encontrado.";
            System.out.println(mensagem);
        }
    }


    public boolean processaTurno() {
        
       /*
        countTurno++;
        boolean test = false;
       Fenomeno fenomeno = fenomenos.get(countTurno);
       for(final Animal atual : animais){
           if(atual.posicao != comprimentoPista){
               test = true;
               atual.tempo ++;
            if(fenomeno != null){
                fenomeno.aplicar(atual);
            }
           atual.calcularDistancia(comprimentoPista - 1, iniciouCorrida, TipoPista);
            
            if(fenomeno != null){
                fenomeno.anulaFenomeno(atual);
            }
            if(atual.posicao == comprimentoPista){
                
            }else if(meta.contains(atual)){
                meta.add(atual);
            }
               
           }
           return test;
       } */
     
        int numJogadores = animais.size();
        int count = 0;

        for (int i = 0; i < numJogadores; i++) {

            if (meta.contains(animais.get(i)) == false) {

                if (animais.get(i).getEnergiaAtual() > 0) {

                    animais.get(i).calcularDistancia(comprimentoPista - 1, iniciouCorrida, TipoPista);
                    animais.get(i).tempo++;

                    if (animais.get(i).getPosicao() == comprimentoPista - 1) {
                        animais.get(i).classificacao += ultimaPosicao;              //ultimaPosicao da pista
                        ultimaPosicao++;
                        meta.add(animais.get(i));
                    }

                } else if (animais.get(i).energiaAtual == 0) {

                    int Temp = Math.min(animais.get(i).getInteligencia() * 1 / 2, animais.get(i).energiaInicial);

                    animais.get(i).energiaAtual += Math.max(Temp, 1);

                    animais.get(i).tempo++;

                }
            }
        }
        iniciouCorrida = true;

        /*       for (Animal a : animais) {

            if (a.getPosicaoAnimal() >= comprimentoPista - 1) {
                count++;
            }
        }*/
        //----------Function----------------
        count = animais.stream()
                .filter((a) -> (a.getPosicaoAnimal() >= comprimentoPista - 1))
                .map((_item) -> 1).reduce(count, Integer::sum);

        return count != numJogadores;
    }

    public List<String> getClassificacaoGeral() {

        String mensagem = " ";
        String titulo = " ";
        String desempates = " ";

        classificacaoGeral = new ArrayList<>();

        titulo = "CLASSIFICACAO GERAL";

        for (int i = 0; i < animais.size(); i++) {
            mensagem = "#" + animais.get(i).classificacao + " " + animais.get(i).getNomeAnimal() + ", " + animais.get(i).verificarIdEspecie() + ", " + animais.get(i).getTempo();
            classificacaoGeral.add(mensagem);
            Collections.sort(classificacaoGeral);
        }
        classificacaoGeral.add(0, titulo);
        return classificacaoGeral;
    }

    public void atribuirPremios() {
        premios.forEach((premio) -> {
            animais.stream()
                    .filter((animal) -> (premio.getPosicaoQueRecebe() == animal.getPosicaoDeChegada()))
                    .forEachOrdered((animal) -> {
                        animal.adicionarPremio(premio);
                    });
        });
    }

    public List<String> getTabelaPremios() {

        atribuirPremios();

        animais.forEach((animal) -> {
            animal.ordenarPremio();
        });

        Animal.selecionarOrdenacao(1);
        Collections.sort(animais);

        List<String> tabelaPremio = new ArrayList<>();

        tabelaPremios.add("TABELA DE PRÉMIOS \n");

        animais.stream()
                .filter((animal) -> (!premios.isEmpty()))
                .forEachOrdered((animal) -> {
                    tabelaPremios.add(animal.imprimirPremios());
                });
        return tabelaPremios;
    }

    public List<String> getClassificacaoEquipas() {

        classificacaoEquipas = new ArrayList<>();
        String titulo = " ";
        String mensagem = " ";

        for (int i = 0; i < meta.size(); i++) {
            animais.get(i).nrPontos = numAnimais - animais.get(i).classificacao + 1;
        }
        titulo = "CLASSIFICACAO POR EQUIPAS";

        for (int i = 0; i < meta.size(); i++) {

            mensagem = "#" + animais.get(i).classificacao + " " + animais.get(i).verificarIdEspecie() + ", " + animais.get(i).nrPontos;
            classificacaoEquipas.add(mensagem);
            Collections.sort(classificacaoEquipas);

        }
        classificacaoEquipas.add(0, titulo);
        System.out.println("" + classificacaoEquipas);
        return classificacaoEquipas;
    }

    public String getBackgroundImagePNG(int idPosicao) {

        String terra = "box-land.png";
        String agua = "box-water.png";
        String lama = "box-mud.png";
        String rochoso = "box-rocks.png";

        if (TipoPista.contains("T")) {
            switch (TipoPista.get(idPosicao)) {
                case "T":
                    return terra;
                case "A":
                    return agua;
                case "L":
                    return lama;
                case "R":
                    return rochoso;
            }
        }
        return terra;
    }

    public List<Animal> getAnimais() {
        return animais;
    }

    public List<Premio> getPremios() {
        return premios;
    }

    public void escreverResultados(String fileName) {

        File ficheiro = new File(fileName);

        try {
            FileWriter escritor = new FileWriter(ficheiro);
            for (String str : getClassificacaoGeral()) {
                escritor.write(str);
            }

            escritor.write("\n\n");

            for (String str : getTabelaPremios()) {
                escritor.write(str);
            }

            escritor.write("\n");

            for (String str : getClassificacaoEquipas()) {
                escritor.write(str);
            }

            escritor.close();
        } catch (IOException ex) {
            Logger.getLogger(Simulador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

/* public void escreverResultados(String filename) {

        try {
            filename = "Resultados.txt";
            try (FileWriter fileWriter = new FileWriter(filename)) {
                for (int i = 0; i < classificacaoGeral.size(); i++) {
                    fileWriter.write(classificacaoGeral.get(i));
                    fileWriter.flush();
                }

                for (int i = 0; i < classificacaoEquipas.size(); i++) {
                    fileWriter.write(classificacaoEquipas.get(i));
                    fileWriter.flush();
                }
            }

        } catch (IOException e) {
            String mensagem = "Erro: o ficheiro " + filename + " não foi encontrado.";
            System.out.println(mensagem);
        }
    }
 */
