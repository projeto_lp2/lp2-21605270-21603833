package pt.ulusofona.lp2.jungleGame;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public abstract class Animal implements Comparable<Animal> {

    protected String nome;
    protected int id;
    protected int idEspecie;
    protected int velocidade;
    protected int energiaInicial;
    protected int energiaAtual;
    protected int posicao;
    private String nomeEspecie;
    protected int tempo;
    protected int classificacao;
    protected int inteligencia;
    protected int atrito;
    protected int nrPontos;
    protected int favoritismo;
    static int nrDeOrdenacao;
    protected int posicaoDeChegada;
    int valorTotalPremios;
    public List<Premio> premios;


    //--------------------------------------------------------------------------    
    Animal(String nome, int id, int idEspecie, int velocidade, int energiaInicial, int inteligencia) {
        this.nome = nome;
        this.id = id;
        this.idEspecie = idEspecie;
        this.velocidade = velocidade;
        this.energiaInicial = energiaInicial;
        this.posicao = 0;
        this.energiaAtual = energiaInicial;
        this.nomeEspecie = nomeEspecie;
        this.tempo = tempo;
        this.classificacao = classificacao;
        this.premios = premios;
        this.inteligencia = inteligencia;
        this.atrito = atrito;
        this.favoritismo = favoritismo;
        this.nrPontos = 0;
    }

    public String verificarIdEspecie() {
        switch (idEspecie) {
            case 0:
                return "Águia";
            case 1:
                return "Girafa";
            case 2:
                return "Gorila";
            case 3:
                return "Humano";
            case 4:
                return "Leão";
            case 5:
                return "Lebre";
            case 6:
                return "Mocho";
            case 7:
                return "Tartaruga";
            case 8:
                return "Tigre";
            case 9:
                return "Zebra";

        }
        return nomeEspecie;
    }


    @Override
    public int compareTo(Animal outro) {
        if (nrDeOrdenacao == -1) {
            return compareToAnimal(outro);
        } else {
            return compareToValorTotal(outro);
        }
    }

    public int compareToAnimal(Animal outro) {

        if (this.posicao == outro.posicao) {
            if (this.favoritismo == outro.favoritismo) {
                if (this.nome.compareTo(outro.nome) == 0) { // nunca irá acontecer
                    return 0;
                } else if (this.nome.compareTo(outro.nome) < 0) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (this.favoritismo < outro.favoritismo) {
                return -1;
            } else {
                return 1;
            }
        } else if (this.posicao < outro.posicao) {
            return -1;
        } else {
            return 1;
        }
    }

    public int compareToEquipa(Animal outro) {

        if (this.idEspecie == outro.idEspecie) {
            return compareToAnimal(outro);
        } else if (this.idEspecie < outro.idEspecie) {
            return -1;
        } else {
            return 1;
        }
    }

    public int compareToValorTotal(Animal outro) {

        if (this.valorTotalPremios == outro.valorTotalPremios) {
            return 0;
        } else if (this.valorTotalPremios < outro.valorTotalPremios) {
            return 1;
        } else {
            return -1;
        }
    }

    public void adicionarPremio(Premio premio) {
        this.premios.add(premio);
        this.valorTotalPremios += premio.getValorPremio();

    }
    
        public void ordenarPremio(){
        
        Collections.sort(premios);
           for(Premio premio:premios){
                if("N".equals(premio.getAcumulavel())){             //situação do acumulavel
                    this.valorTotalPremios = premio.getValorPremio();
                    break;
                }
            }
    }
        
            public String imprimirPremios(){
        String resultado = "";
       
        for(Premio premio:premios){
                if("N".equals(premio.getAcumulavel())){
                    resultado = premio.imprimirPremio();
                    break;
                }
            resultado += premio.imprimirPremio();
        }
        return nome +" "+ valorTotalPremios + "\n" + resultado + "\n";
    }

    public abstract void calcularDistancia(int comprimentoPista, boolean iniciouCorrida, List<String> TipoPista);

    
    public int acabarCorrida(int comprimentoPista){
        return posicao = comprimentoPista;
    }
    
    public int getPosicaoDeChegada() {
        return this.posicaoDeChegada;
    }

    public void setPosicaoDeChegada(int valor) {
        this.posicaoDeChegada = valor;
    }

    public String getNomeEspecie() {
        return nomeEspecie;
    }

    public int getPosicao() {
        return posicao;
    }

    public String getImagePNG() {
        return null;
    }

    public String getNomeAnimal() {
        return nome;
    }

    public int getVelocidade() {
        return velocidade;
    }

    public void setVelocidade(int velocidade) {
        this.velocidade = velocidade;
    }
    

    public int getPosicaoAnimal() {
        return posicao;
    }

    public int getEnergiaAtual() {
        return energiaAtual;
    }

    public void setEnergiaAtual(int energiaAtual) {
        this.energiaAtual = energiaAtual;
    }
    

    public int getTempo() {
        return tempo;
    }

    public int getIdEspecie() {
        return idEspecie;
    }

    public int getInteligencia() {
        return inteligencia;
    }

    public void setInteligencia(int inteligencia) {
        this.inteligencia = inteligencia;
    }
 

    public int getFavoritismo() {
        return favoritismo;
    }
     
    
   public static void selecionarOrdenacao(int valor){
        nrDeOrdenacao = valor;
    }

    @Override
    public String toString() {
        return "Animal(" + "nome=" + nome + ", id=" + id + ", idEspecie=" + idEspecie + ", energiaAtual=" + energiaAtual + ", classificacao=" + classificacao + ", favoritismo=" + favoritismo + ", premios=" + premios + ')';
    }
    
    

}
