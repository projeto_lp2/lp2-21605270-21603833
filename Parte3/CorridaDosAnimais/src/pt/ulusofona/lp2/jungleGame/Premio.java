package pt.ulusofona.lp2.jungleGame;

public class Premio implements Comparable<Premio>{
    String nome;
    int valor;
    int posicaoAtual;
    String acumulavel; //mudar para char
    
    Premio (String nome, int valor, int posicaoAtual, String acumulavel){
        this.nome = nome;
        this.valor = valor;
        this.posicaoAtual = posicaoAtual;
        this.acumulavel = acumulavel;
    }
    
    int getValorPremio(){
        return this.valor;
    }
    
    int getPosicaoQueRecebe(){
        return this.posicaoAtual;
    }
    
    String getNomePremio() {
        return this.nome;
    }
    
    String getAcumulavel(){
        return this.acumulavel;
    }
    
    
    
    @Override
    public int compareTo(Premio other){
        if((this.acumulavel == null ? other.acumulavel == null : this.acumulavel.equals(other.acumulavel)) && "S".equals(this.acumulavel)){
            if(this.valor > other.valor){
                return -1;
            }else{
                return 1;
            }
        }else if((this.acumulavel == null ? other.acumulavel == null : this.acumulavel.equals(other.acumulavel)) && "N".equals(this.acumulavel)){
            if(this.valor > other.valor){
                return -1;
            }else{
                return 1;
            }
        }else if("S".equals(this.acumulavel) && "N".equals(other.acumulavel)){
            return 1;
        }else{
            return -1;
        }
    }
    
 
    public String imprimirPremio() {
        return "  " + nome + " (" + valor + ") \n";
    }
    
}
