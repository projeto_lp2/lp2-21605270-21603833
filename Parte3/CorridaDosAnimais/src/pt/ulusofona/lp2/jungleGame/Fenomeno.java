package pt.ulusofona.lp2.jungleGame;

import java.util.function.Consumer;
import java.util.function.Function;

public class Fenomeno {

    private Animal animal;
    Consumer<Animal> consumidor;
    private int idFenomenos;
    private int turno;

    private int inteligenciaTemporaria;
    private int energiaTemporaria;
    private int velocidadeTemporaria;

    Function<Integer, Boolean> testarInteligencia = (inteligenciaTemp) -> inteligenciaTemp > 1;
    Function<Integer, Boolean> testarVelocidade = (velocidadeTemp) -> velocidadeTemp > 1;
    Function<Integer, Boolean> testarVelocidade2 = (velocidadeTemp) -> velocidadeTemp > 4;
    Function<Integer, Integer> diminuirInteligencia = (inteligenciaTemp) -> inteligenciaTemp - 1;
    Function<Integer, Integer> duplicarEnergia = (energiaTemp) -> energiaTemp * 2;
    Function<Integer, Integer> aumentarVelocidade = (velocidadeTemp) -> velocidadeTemp + 1;
    Function<Integer, Integer> aumentarVelocidade2 = (velocidadeTemp) -> velocidadeTemp + 2;
    Function<Integer, Integer> diminuirVelocidade = (velocidadeTemp) -> velocidadeTemp - 1;

    public Fenomeno(int idFenomenos, int turno) {
        this.idFenomenos = idFenomenos;
        this.turno = turno;
    }

    void criarFenomenos(int idFenomenos) {
        switch (idFenomenos) {
            case 1:
                if (testarInteligencia.apply(animal.getInteligencia())) {
                    diminuirInteligencia.apply(animal.getInteligencia());
                }
                break;
            case 2:
                duplicarEnergia.apply(animal.getEnergiaAtual());
                break;
            case 3:
                aumentarVelocidade.apply(animal.getEnergiaAtual());
                break;
            case 4:
                if (testarVelocidade.apply(animal.getEnergiaAtual())) {
                    diminuirVelocidade.apply(animal.getEnergiaAtual());
                }
                break;
            case 5:
                if (testarVelocidade2.apply(animal.getEnergiaAtual())) {
                    aumentarVelocidade2.apply(animal.getEnergiaAtual());
                }
                break;

        }
    }
    
        public void aplicar(final Animal animal) {
        if (animal == null) {
            throw new NullPointerException("Nunca é null");
        }
        this.animal = animal;
        inteligenciaTemporaria = animal.getInteligencia();
        energiaTemporaria = animal.getEnergiaAtual();
        velocidadeTemporaria = animal.getVelocidade();
        consumidor.accept(animal);

    }

    public void anulaFenomeno(final Animal animal) {
        if (animal == null) {
            throw new NullPointerException("Nunca é null");
        }
        if (this.animal != animal) {
            throw new IllegalStateException("a funçao [aplicar] tem de ser efectuada priemeiro ");
        }
        animal.setInteligencia(inteligenciaTemporaria);
        animal.setEnergiaAtual(energiaTemporaria);
        animal.setVelocidade(velocidadeTemporaria);
        this.animal = null;
    }

    public int getIdFenomenos() {
        return idFenomenos;
    }

    public void setIdFenomenos(int idFenomenos) {
        this.idFenomenos = idFenomenos;
    }

    public int getTurno() {
        return turno;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }

    public int getInteligenciaTemporaria() {
        return inteligenciaTemporaria;
    }

    public void setInteligenciaTemporaria(int inteligenciaTemporaria) {
        this.inteligenciaTemporaria = inteligenciaTemporaria;
    }

    public int getEnergiaTemporaria() {
        return energiaTemporaria;
    }

    public void setEnergiaTemporaria(int energiaTemporaria) {
        this.energiaTemporaria = energiaTemporaria;
    }

    public int getVelocidadeTemporaria() {
        return velocidadeTemporaria;
    }

    public void setVelocidadeTemporaria(int velocidadeTemporaria) {
        this.velocidadeTemporaria = velocidadeTemporaria;
    }

}
