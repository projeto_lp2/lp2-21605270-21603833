package pt.ulusofona.lp2.jungleGame;

import static java.lang.Math.floor;
import java.util.List;

public class Girafa extends Animal {

    public Girafa(String nome, int id, int idEspecie, int velocidade, int energiaInicial, int inteligencia) {
        super(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
        this.idEspecie = 1;
    }

    @Override
    public void calcularDistancia(int comprimentoPista, boolean iniciouCorrida, List<String> TipoPista) {
        

        if (TipoPista.get(posicao).equals("R") || TipoPista.get(posicao).equals("T")) {
            atrito = 0;
        } else {
            atrito = 1;
        }

        int distancia = (int) (velocidade * energiaAtual * 0.5 + 1 - atrito);

        if (posicao + distancia <= comprimentoPista) {
            posicao += distancia;
            int temp = (int) floor(distancia - inteligencia * 0.5);
            energiaAtual -= Math.max(temp, 1);
        } else {
            posicao = comprimentoPista;

        }

    }

}
