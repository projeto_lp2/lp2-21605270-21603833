package pt.ulusofona.lp2.jungleGame;

import static java.lang.Math.floor;
import java.util.List;

public class Tartaruga extends Animal {

    public Tartaruga(String nome, int id, int idEspecie, int velocidade, int energiaInicial, int inteligencia) {
        super(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);
        
    }

    @Override
    public void calcularDistancia(int comprimentoPista, boolean iniciouCorrida, List<String> TipoPista) {
        

        if (TipoPista.get(posicao).equals("A")) {
            atrito = 0;
            posicao++;
        } else {
            atrito = 1;
        }

        int distancia = (int) (velocidade * energiaAtual * 0.5 + 1 - atrito);

        if (posicao + distancia <= comprimentoPista) {
            posicao += distancia;
            int temp = (int) floor(distancia - inteligencia * 1 / 2);
            energiaAtual -= Math.max(temp, 1);

        } else {
            posicao = comprimentoPista;

        }

    }
}
