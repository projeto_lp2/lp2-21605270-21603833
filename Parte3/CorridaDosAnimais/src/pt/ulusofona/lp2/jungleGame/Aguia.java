package pt.ulusofona.lp2.jungleGame;

import static java.lang.Math.floor;
import java.util.List;

public class Aguia extends Animal {

    public Aguia(String nome, int id, int idEspecie, int velocidade, int energiaInicial, int inteligencia) {

        super(nome, id, idEspecie, velocidade, energiaInicial, inteligencia);

    }

    @Override
    public void calcularDistancia(int comprimentoPista, boolean iniciouCorrida, List<String> TipoPista) {
        
         

        int distancia = (int) (velocidade * energiaAtual * 0.5 + 1 - atrito);

        //int energiaGasta = 0;
        if (posicao + distancia <= comprimentoPista) {
            posicao += distancia;
            int temp = (int) floor(distancia - inteligencia * 1 / 2);
            energiaAtual -= Math.max(temp, 1);
        } else {
            posicao = comprimentoPista;

        }

    }

}

