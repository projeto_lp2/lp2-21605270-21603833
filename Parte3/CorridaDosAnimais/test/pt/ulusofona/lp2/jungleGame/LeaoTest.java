package pt.ulusofona.lp2.jungleGame;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class LeaoTest {
    
    
    @Test
    public void testCalcularDistancia() {
        Girafa g = new Girafa("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("T");
        tipoPista.add("T");
        
        
        g.calcularDistancia(2, true, tipoPista);
        int resultado = 6;
        
        int esperado = 6;
        
        
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testCalcularDistancia2() {
        Girafa g = new Girafa("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("R");
        tipoPista.add("T");
        
        
        g.calcularDistancia(2, true, tipoPista);
        int resultado = 6;
        
        int esperado = 6;
        
        
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testCalcularDistancia3() {
        Girafa g = new Girafa("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("R");
        tipoPista.add("R");
        
        
        g.calcularDistancia(2, true, tipoPista);
        int resultado = 6;
        
        int esperado = 5;
        
        
        
        assertEquals(esperado, resultado);
    }
    
}
