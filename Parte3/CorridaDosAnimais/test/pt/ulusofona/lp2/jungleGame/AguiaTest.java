package pt.ulusofona.lp2.jungleGame;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class AguiaTest {
    
    
    @Test
    public void testCalcularDistancia() {
        Aguia ag = new Aguia("ninja", 2, 7, 5, 2, 2);
        
        List<String> tipoPista = new ArrayList<String>();
        tipoPista.add("T");
        tipoPista.add("T");
        
        
        ag.calcularDistancia(2, true, tipoPista);
        int resultado = 6;
        
        int esperado = 6;
        
        
        
        assertEquals(esperado, resultado);
    }
    
}
